package webservice;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.google.gson.Gson;

import dao.ConnectionManager;
import dto.StockObjects;
import model.Stocks;

@Path("/Service")
public class StockService {

	@OPTIONS
	public Response myResource() {
	    return Response.ok().build();
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response stock() throws SQLException{
		String stocks = null;

		
		try {
			System.out.println("------------");
			List<StockObjects> stockData = new ArrayList<>();

			
			Gson gson = new Gson();
			 System.out.println(gson.toJson(stockData)); 
			 stocks = gson.toJson(stockData);
			
		}

		catch (Exception e) {
			//System.out.println("Exception Error");
			e.printStackTrace();// Console
		}

		return Response.ok() //200
				.entity(stocks)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.allow("OPTIONS").build();
	}
	
	
	@GET
	@Path("Stock")
	@Produces(MediaType.APPLICATION_JSON)
	public String stock(@QueryParam("id") int id){
		String stocks = null;
		
		//CORSFilter corsFilter= new CORSFilter();
		//corsFilter.filter(request, response); 
		
		
			System.out.println("------------");
			List<StockObjects> stockData = new ArrayList<>();

			
			//ConnectionManager connectionManager = new ConnectionManager();
			Stocks stock= new Stocks();
			 try {
				stockData = stock.getStocks(id);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Gson gson = new Gson();
			 System.out.println(gson.toJson(stockData.get(0))); 
			 stocks = gson.toJson(stockData.get(0));
			
		

		

		return stocks;
	}
	
	@GET
	@Path("GetSuggestion")
	@Produces(MediaType.APPLICATION_JSON)
	public String stock(@QueryParam("text") String text){
		String stocks = null;

		
		try {
			System.out.println("------------");
			List<StockObjects> stockData = new ArrayList<>();

			
			//ConnectionManager connectionManager = new ConnectionManager();
			Stocks stock= new Stocks();
			stockData=stock.getSearchText(text);
			
			Gson gson = new Gson();
			 System.out.println(gson.toJson(stockData)); 
			 stocks = gson.toJson(stockData);
			
		}

		catch (Exception e) {
			//System.out.println("Exception Error");
			e.printStackTrace();// Console
		}
		

		return stocks;
	}
}
