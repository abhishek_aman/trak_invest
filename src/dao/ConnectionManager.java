package dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import dto.StockObjects;
import model.Stocks;

public class ConnectionManager {

	private Database database;
	private Connection connection;
	private static ConnectionManager _instance;

	ConnectionManager() {
		ConnectToDB();

	}

	private boolean ConnectToDB() {

		try {

			database = new Database();
			connection = database.Get_Connection();
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	public Connection getConnectionInstance() {

		return connection;
	}

	public static ConnectionManager getConnectionManagerInstance() {

		if (_instance == null) {
			_instance = new ConnectionManager();
		}

		return _instance;
	}
}
