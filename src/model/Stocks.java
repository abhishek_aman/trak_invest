package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.ConnectionManager;
import dto.StockObjects;;

public class Stocks {
	private ConnectionManager connectionManager;
	private Connection connection;

	public Stocks() {

		connectionManager = ConnectionManager.getConnectionManagerInstance();
		connection = connectionManager.getConnectionInstance();
	}

	public List<StockObjects> getStocks(int id) throws Exception {
		List<StockObjects> stockData = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		// String selectSQL = "SELECT company_name, industry, symbol, series, isin_code
		// FROM trakinvest_data WHERE id = ?";
		try {
		
			ps = connection.prepareStatement("select * from demo.trakinvest_data where id = ?");
			ps.setInt(1, id);
			rs = queryExecutor(ps);
			while (rs.next()) {
				StockObjects stockObject = new StockObjects();
				stockObject.setId(id);
				stockObject.setCompanyName(rs.getString("company_name"));
				stockObject.setIndustry(rs.getString("industry"));
				stockObject.setIsinCode(rs.getString("isin_code"));
				stockObject.setSeries(rs.getString("series"));
				stockObject.setSymbol(rs.getString("symbol"));

				stockData.add(stockObject);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return stockData;

	}

	public List<StockObjects> getSearchText(String text) throws Exception {
		List<StockObjects> stockData = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "select * from demo.trakinvest_data where company_name LIKE '%" + text + "%'";

		ps = connection.prepareStatement(query);
		// ps.setString(1, text);
		rs = queryExecutor(ps);

		while (rs.next()) {
			StockObjects stockObject = new StockObjects();
			stockObject.setId(rs.getInt(6));
			stockObject.setCompanyName(rs.getString("company_name"));
			stockObject.setIndustry(rs.getString("industry"));
			stockObject.setIsinCode(rs.getString("isin_code"));
			stockObject.setSeries(rs.getString("series"));
			stockObject.setSymbol(rs.getString("symbol"));

			stockData.add(stockObject);
		}
		return stockData;
	}

	private ResultSet queryExecutor(PreparedStatement ps) throws Exception {
		ResultSet rs = null;

		try {
			rs = ps.executeQuery();
		} catch (Exception es) {
			throw es;
		}

		return rs;
	}

}
